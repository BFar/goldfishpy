'''
Created on Jan 7, 2015

@author: bryanfarris
'''
import goldfish.config as config
from pysendy import Sendy

class Email ():
    
    @classmethod
    def subscribe (cls, **kwargs):
        """
        Subscribes user to a Sendy email list.  
        Defaults to the welcome email list if no list_id is specified.
        
        Required Kwargs: name, email, 
        
        Optional Kwargs: list_id, any custom params
        
        """
        #Retrieve Kwargs
        s = Sendy(base_url=config.SENDY_BASE_URL, api_key=config.SENDY_API_KEY)
        name = kwargs.get('name')
        email = kwargs.get('email')
        list_id = kwargs.get('list_id')
        
        #Ensure defaults
        if list_id is None:
            list_id = config.SENDY_LIST_IDs.get('default')
        else:
            list_id = config.SENDY_LIST_IDs.get(list_id,list_id) 
            #Check if list_id matches a key in the sendy_list_ids config dict, if not, it must be a list_id
        
        #TODO- add support for other lists (pass list key to list id)
        
        if name is None or email is None or list_id is None: #ERROR PROTECTION
            return
        
        #APPEND CUSTOM PARAMS
        params = {}
        for key in kwargs:
            if key != 'name' and key != 'email' and key != 'list_id':
                params[key] = kwargs.get(key)
        
        
        #Subscribe to Sendy:
        s.subscribe(name=name, email=email, list_id=list_id, **params)
        
        return 'Success! '+name+ ' is now subscribed as ' + email + ' to list with id: '+ list_id