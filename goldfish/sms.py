'''
Created on Jan 6, 2015

@author: bryanfarris
'''

import goldfish.config as config
import goldfish.tasks as tasks
#from goldfish import config
#from goldfish import tasks

#import importlib
#config = importlib.import_module('goldfish.config')
#tasks = importlib.import_module('goldfish.tasks')

import twilio
from twilio.rest import TwilioRestClient


class SMS(): #ModelResource):
    
    @classmethod 
    def send(cls, **kwargs):
        """
        Accepts a pre-formated phone number and a message, then attempts to send an sms
        to the recipient.  This is executed within a try: statement because it is subject to fail 
        due to anything from an incorrectly formatted phone number to an empty Twilio 
        account balance.
        """
        
        #Gather Kwargs
        to_num = kwargs.get('to_num','+18054040899')
        msg = kwargs.get('msg','Oops')

        try:
            #DECLARE TWILIO CLIENT
            account_sid = config.TWILIO_ACCOUNT_SID
            auth_token = config.TWILIO_AUTH_TOKEN
            client = TwilioRestClient(account_sid, auth_token)
            
            from_num = config.TWILIO_FROM_NUM
            
            print("Sending Twilio SMS",to_num,from_num,msg)
            message = client.messages.create(to=to_num, from_=from_num, body=msg)
            
#            print(message.sid) #TODO Add back in some form of logging
        except:
            return
        
    @classmethod
    def delay (cls, seconds, **kwargs):
        """
        Schedules an SMS to fire on delay
        """
        #Gather Kwargs
        to_num = kwargs.get('to_num','+18054040899')
        msg = kwargs.get('msg')
        
        classname = 'goldfish.sms.SMS'
        tasks.FireMethodTask.apply_async(countdown=seconds,
                                         args = [classname,'send'],
                                         kwargs={'to_num':to_num,
                                                 'msg':msg}
                                         )
        
        
        