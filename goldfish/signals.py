from django.dispatch import Signal


pre_create = Signal(providing_args=["instance", "hashkey", "data", "args", "kwargs"], use_caching=True)
post_update = Signal(providing_args=["instance", "hashkey", "data", "args", "kwargs"], use_caching=True)
post_delete = Signal(providing_args=["instance", "hashkey", "args", "kwargs"], use_caching=True)