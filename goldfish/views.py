'''
Created on Jan 6, 2015

@author: bryanfarris
'''
#from django.contrib.auth import logout
#from django.http import HttpResponseRedirect, HttpResponse
#from django.contrib.auth.decorators import login_required
#from django.utils.decorators import method_decorator
#
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework.authentication import BasicAuthentication,\
#    SessionAuthentication
from rest_framework.permissions import AllowAny
#
#
##PUSH
from goldfish.pushnotifs import push
#
##REDIS
from goldfish.store import redis

from goldfish.tasks import UpdateResource

#from rest_framework.permissions import IsAuthenticated
#
## Provider OAuth2
#from provider.oauth2.models import Client

import json
#import vobject

import goldfish.config as config
from goldfish.sms import SMS
from goldfish.email import Email
from goldfish.managers import ResourceManager
from goldfish.managers import RedisHelper
from goldfish.managers import APIManager
#import importlib
#SMS = importlib.import_module('goldfish.sms.SMS')
#Email = importlib.import_module('goldfish.email').Email

#class LoginRequiredMixin(object):
#    @method_decorator(login_required)
#    def dispatch(self, request, *args, **kwargs):
#        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class TestView (APIView):
    def get(self, request, *args, **kwargs):
        
#        #RETURN VCARD
#        j = vobject.vCard()
#        j.add('n')
#        j.n.value = vobject.vcard.Name( family='Harris', given='Jeffrey' )
#        j.add('fn')
#        j.fn.value ='Jeffrey Harris'
#        j.add('email')
#        j.email.value = 'jeffrey@osafoundation.org'
#        j.email.type_param = 'INTERNET'
#        
#        
#        j.prettyPrint()
#        
#        return Response(j.serialize(),status=HTTP_200_OK)
        #COMMUNICATIONS TEST
#        test = 'HELLO GOLDFISH WORLD'
##        SMS.send(msg=test)
#        
#        SMS.delay(10, msg=test)
#        
#        Email.subscribe(name="BFar", email="BryanFarris@gmail.com", message=test)
#        
#        test = 'hello'
        test = APIManager.post_activity()
        return Response(test, status = status.HTTP_200_OK)
#        
        
class ResourceView (APIView):
    permission_classes = (AllowAny,)
#        envelope = request.GET.get('envelope')
#    authentication_classes = (SessionAuthentication, BasicAuthentication,)
#    permission_classes = (IsAuthenticated,)
    
    def get(self, request, *args, **kwargs):
        """
        Validate GET request and return resource
        """
        #Validate required args
        resource = kwargs.get('resource')

        filters = self.request_params(request,list_keys=['include','exclude'])
        kwargs.update(filters)

        has_element = self.has_element(**kwargs) #TODO Should this be in the view? why not in get_resource somewhere? what is this?
        kwargs["has_element"] = has_element

        if resource is None:
            return Response(status = status.HTTP_400_BAD_REQUEST)
        
        response = APIManager.get_resource(*args, **kwargs)
#        response = self.get_resource(self, *args, **kwargs)
        
        if response is None:
            return Response(status = status.HTTP_204_NO_CONTENT) #No object
            
        return Response(response, status = status.HTTP_200_OK)
    
    
#    resource = kwargs.pop('resource')
#        id_field = kwargs.pop('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = self.prefix(**kwargs)

#        filters = self.request_params(request,list_keys=['include','exclude'])
#        kwargs.update(filters)
#        
#        if resource is None:
#            return Response(status = status.HTTP_400_BAD_REQUEST)
#        elif resource_id is None:
#            return Response(ResourceManager.list_resources(prefix,resource, **kwargs))#**filters))
#        
#        obj = ResourceManager.get_resource(prefix, resource,resource_id, **kwargs)#**filters)
#        if obj is None:
#            return Response(status = status.HTTP_204_NO_CONTENT) #No object
#            
#        return Response(obj, status = status.HTTP_200_OK)     
    
#    @classmethod
#    def get_resource(cls, *args, **kwargs):
#        """
#        Validate GET request and return resource
#        
#        Required Kwargs:
#        -- resource
#        -- id_field
#        
#        Optional Kwargs:
#        -- resource_id (passed as value of id_field)
#        
#        """
#        #Validate required args
#        resource = kwargs.pop('resource')
#        id_field = kwargs.pop('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = cls.prefix(**kwargs)
#        
#        if resource is None:
#            return None
#        elif resource_id is None:
#            return ResourceManager.list_resources(prefix,resource, **kwargs)
#        
#        return ResourceManager.get_resource(prefix, resource,resource_id, **kwargs)
        
       
       
    def delete(self, request, *args, **kwargs):
        """
        Validate DELETE request and delete resource
        
        """
        
        #Validate required args
        resource = kwargs.get('resource')
        id_field = kwargs.get('id_field')
        resource_id = kwargs.get(id_field)
        prefix = self.prefix(**kwargs) 
        
#        if resource is None or resource_id is None:
#            return Response(status = status.HTTP_400_BAD_REQUEST)
#        
#        return Response(ResourceManager.delete_resource(prefix, resource, resource_id))
    
        if kwargs.get('prefix') is not None: # ERROR PROTECTION FOR KWARGS:
            kwargs.pop('prefix')
        if kwargs.get('resource') is not None:
            kwargs.pop('resource')
        if kwargs.get('resource_id') is not None:
            kwargs.pop('resource_id')
    
        if resource is None:
            return Response(status = status.HTTP_400_BAD_REQUEST)
        elif resource_id is not None:
            return Response(ResourceManager.delete_resource(prefix, resource, resource_id, **kwargs))
        else:
            resource_ids = [] # RETRIEVE IDS FOR EXISTING RESOURCE OBJECTS
            resource_list = ResourceManager.list_resources(prefix,resource, style='id', 
                                                           exclude=kwargs.get('exclude'), 
                                                           include=kwargs.get('include'))
            if resource_list is not None:
                resource_ids = resource_list.get(resource.upper())
            
            resources = []
            for item_id in resource_ids:
                # DELETE EXISTING RESOURCE
                r = ResourceManager.delete_resource(prefix, resource, item_id, **kwargs)
                if r is not None:
                    resources.append(r)
            
            if len(resource_ids) == 0:
                msg = 'NO RESOURCES TO DELETE'
                return Response(msg, status=status.HTTP_304_NOT_MODIFIED)
            elif len(resources) != 0:
                msg = 'ERROR: ' + str(len(resources)) + ' ' + resource.upper() +' resources remain out of ' + str(len(resource_ids)) +  ' ids found.'
                return Response(msg, status = status.HTTP_206_PARTIAL_CONTENT)
            
            msg = 'SUCCESSFULLY DELETED ' + str(len(resource_ids)) + ' ' + resource.upper() + ' ITEMS!'
            return Response(msg, status = status.HTTP_200_OK) 
        
    def update(self, request, overwrite, *args, **kwargs):
        """
        Execute PUT/POST request to create/update resource.
        
        """
        #Validate required args
        resource = kwargs.get('resource')
        
        if resource is None:
            return None
        
        decoded = request.body.decode('utf8')
        data = json.loads(decoded)
        
        query_params = request.GET.dict()
        
        if query_params.get('format') is not None:
            query_params.pop('format') #TODO: ADD QUERY PARAMS VAR TO STORE OBJECT TO PREVENT THIS --> DONE, TEST AND REMOVE
        
        
#        resources = UpdateResource.delay(query_params, data, overwrite, *args, **kwargs)
    
        if query_params.get('async') is not None:
            UpdateResource.delay(query_params, data, overwrite, *args, **kwargs)
            return 'Successfully posted ' + resource + ' object!'

        resources = APIManager.update_resource(query_params, data, overwrite, *args, **kwargs)
            
        return resources
    
    
#        #Validate required args
#        resource = kwargs.get('resource')
#        id_field = kwargs.get('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = self.prefix(**kwargs) 
#        style = kwargs.get('style')
#        
#        if resource is None:
#            return None
#        
#        decoded = request.body.decode('utf8')
#        data = json.loads(decoded)
#        
#        items = []
#        if isinstance(data, list): #HANDLE ARRAY POST
#            items = data
#        elif isinstance(data, dict): #HANDLE DICT POST/PUT
#            items = [data]
#            
#        resources = []
#        for item in items:
#            #UPDATE RESOURCE
#            params = request.GET.dict()  #TODO: SHOULDN'T QUERY PARAMS BE PASSED SEPARATELY?
#            item.update(params)
#            obj = ResourceManager.update_resource(prefix, 
#                                                  resource, 
#                                                  id_field, 
#                                                  resource_id, 
#                                                  overwrite,
#                                                  style, 
#                                                  **item)
#            if obj is not None:
#                resources.append(obj)
#        
#        if resources == []:
#            return None
#        elif isinstance(data, dict): #HANDLE DICT POST/PUT
#            return resources[0]
#
#        return resources   
    
    
    def put (self, request, *args, **kwargs):
        """
        Validate PUT request to create/update resource.
        
        """
        #Validate required args
        resource = kwargs.get('resource')
        id_field = kwargs.get('id_field')
        resource_id = kwargs.get(id_field)
        
        if resource is None or resource_id is None:
            return Response(status = status.HTTP_400_BAD_REQUEST)
        
        resource_item = self.update(request, False, *args, **kwargs)
        if resource_item is not None:
            return Response(resource_item,status=status.HTTP_201_CREATED)

        return Response(status = status.HTTP_204_NO_CONTENT)

        
    def post(self, request, *args, **kwargs):
        """
        Validate POST request to create/overwrite resource.
        
        """
        
        #Validate required args
        resource = kwargs.get('resource')
        
        if resource is None:
            return Response(status = status.HTTP_400_BAD_REQUEST)
    
        resources = self.update(request, True, *args, **kwargs)
        
        #ALTERNATIVE STATUS IF NO RESULTS      
        if resources is None:
            return Response(status = status.HTTP_204_NO_CONTENT)
        
        return Response(resources, status = status.HTTP_201_CREATED)
    
    
    
    
    
#    def put (self, request, *args, **kwargs):
#        """
#        Validate PUT request to create/update resource.
#        
#        """
#        #Validate required args
#        resource = kwargs.get('resource')
#        id_field = kwargs.get('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = self.prefix(**kwargs) 
#        
#        if resource is None or resource_id is None:
#            return Response(status = status.HTTP_400_BAD_REQUEST)
#        
#        decoded = request.body.decode('utf8')
#        data = json.loads(decoded)
#        if isinstance(data, dict):
#            #APPEND PARAMS:
#            params = request.GET.dict()
#            data.update(params)
#            
#            return Response(
#                            ResourceManager.update_resource(prefix, resource, id_field, resource_id, **data), 
#                            status=status.HTTP_201_CREATED
#                            )
#
#        return Response(status = status.HTTP_204_NO_CONTENT)
#        
#    def post(self, request, *args, **kwargs):
#        """
#        Validate POST request to create/overwrite resource.
#        
#        """
#        
#        #Validate required args
#        resource = kwargs.get('resource')
#        id_field = kwargs.get('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = self.prefix(**kwargs) 
#        
#        
#        if resource is None:
#            return Response(status = status.HTTP_400_BAD_REQUEST)
##        elif resource_id is None:
##            return Response(ResourceManager.list_resources(prefix,resource, **filters))
#        
#        
#        
#        decoded = request.body.decode('utf8')
#        data = json.loads(decoded)#request.POST.dict()#json.loads(request.body)
#        
#        items = []
#        if isinstance(data, list): #HANDLE ARRAY POST
#            items = data
#        elif isinstance(data, dict): #HANDLE DICT POST
#            items = [data]
#        
#        resources = []
#        for item in items:
#            #DETERMINE RESOURCE ID: #TO SIMPLIFY & DEPRECATE
#            item_id = resource_id #TO DEPRECATE: JUST USE NEXT ID & STORE IT TO ID FIELD.  MOVE THIS OUT OF VIEWS.
#            if item_id is None:  #Default to ID in url if included
#                if item.get(id_field) is not None:  #If no id in URL, check if it exists in resource body
#                    item_id = item.get(id_field)
#                else:
#                    item_id = ResourceManager.next_id(prefix, resource) #Otherwise generate resource id with redis incr
#            
#            #DELETE EXISTING RESOURCE TO ENSURE OVERWRITE
#            ResourceManager.delete_resource(prefix, resource, item_id)
#            
#            #POST RESOURCE
##            obj = self.post_resource(item, request, prefix, resource, id_field, item_id)
#            params = request.GET.dict()
#            item.update(params)
#            obj = ResourceManager.update_resource(prefix, resource, id_field, item_id, **item)
#            if obj is not None:
#                resources.append(obj)  
#        
#        #ALTERNATIVE STATUS IF NO RESULTS      
#        if resources == []:
#            return Response(status = status.HTTP_204_NO_CONTENT)
#        
#        return Response(resources, status = status.HTTP_201_CREATED)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
#        items = []
#        if isinstance(data, list): #HANDLE ARRAY POST
#            for item in data:
#                resource_id = self.next_id(resource)
#                obj = self.post_resource(item, request, prefix, resource, id_field, resource_id)
#                if obj is not None:
#                    items.append(obj)
#        elif isinstance(data, dict): #HANDLE DICT POST (EXPECTS ID IN URL)
#            if resource_id is None:
#                resource_id = self.next_id(resource) #IF NO ID IS SPECIFIED & DICT IS OFFERED, USE NEXT AVAILABLE ID
##                return Response(status = status.HTTP_400_BAD_REQUEST)
#            
#            obj = self.post_resource(data, request, prefix, resource, id_field, resource_id)
#            if obj is not None:
#                items.append(obj)
                
        
#    def post_resource(self, item, request, prefix, resource, id_field, resource_id, **kwargs):
#        """
#        POSTS a specific resource.  
#        """
#        params = request.GET.dict()
#        item.update(params)
#        return ResourceManager.update_resource(prefix, resource, id_field, resource_id, **item)
        
        
        
        
        
        
        
        
        
        #Logging:
#        if config.ENVIRONMENT != 'production':
#            print 'POST RECEIVED with body: '
#            print request.body.encode('ascii', 'ignore')
        
#        #Validate required args
#        resource = kwargs.get('resource')
#        id_field = kwargs.get('id_field')
#        resource_id = kwargs.get(id_field)
#        prefix = self.prefix(**kwargs) 
#        
#        
#        
#        if resource is None or resource_id is None:
#            return Response(status = status.HTTP_400_BAD_REQUEST)
#        
#        decoded = request.body.decode('utf8')
#        data = json.loads(decoded)#request.POST.dict()#json.loads(request.body)
#        if isinstance(data, dict):
#            #APPEND PARAMS:
#            params = request.GET.dict()
#            data.update(params)
#            
#            #EXCLUDE PARAMS
#            #id_field?
#
#            return Response(
#                            ResourceManager.update_resource(prefix, resource, id_field, resource_id, **data), 
#                            status=status.HTTP_201_CREATED
#                            )
#
#        return Response(status = status.HTTP_204_NO_CONTENT)
    
    
    
    @classmethod
    def prefix (cls, **kwargs):
        """
        Return a resource prefix.
        
        Required Args/Kwargs:
        -- resource_tree
        """
        
        resource_tree = kwargs.get('resource_tree')
        prefix = ''
        
        if resource_tree is not None:
            for resource_dict in resource_tree:
                if isinstance(resource_dict, dict):
                    for resource, id_field in resource_dict.items():
                        resource_id = kwargs.get(id_field)
                        prefix = ResourceManager.hashkey(prefix, resource, resource_id) + ':'
                elif isinstance(resource_dict, str):
                    prefix = prefix + resource_dict + ':'
                    
#        if resource_tree is not None:
#            for resource, id_field in resource_tree.items():
#                resource_id = kwargs.get(id_field)
#                prefix = ResourceManager.hashkey(prefix, resource, resource_id) + ':'
            
        return prefix

    @classmethod
    def has_element (self, **kwargs):
        has_elems = []

        has_list = kwargs.get("has_element")
        if has_list is not None and isinstance(has_list, (list,dict)):
            if isinstance(has_list, dict):
                has_list = [has_list]

            for has_dict in has_list:
                new_has_dict = {}
                for field, id_field in has_dict.items():
                    if kwargs.get(id_field) is not None:
                        new_has_dict[field] = kwargs.get(id_field)
                if bool(has_dict):
                    has_elems.append(new_has_dict)

        return has_elems
    
    def query_params (self, params, **kwargs):
        list_keys = kwargs.get('list_keys',[])
        delimiter = kwargs.get('delimiter','|')
        
        for key in list_keys:
            l = params.get(key)
            if l is not None:
                params[key] = l.split(delimiter)
            
        return params
        
    def request_params (self, request, **kwargs):
        list_keys = kwargs.get('list_keys',[])
        delimiter = kwargs.get('delimiter','|')
        
        kws = request.GET.dict()
        
        for key in list_keys:
            l = request.GET.get(key)
            if l is not None:
                kws[key] = l.split(delimiter)
            
        return kws
    
#    def next_id (self, resource, **kwargs):
#        """
#        Returns next id for a specific resource given redis incr.
#        """
#        key = resource + ':ids'
#        return str(redis.incr(key))

class RedisView(APIView):
    
#    @login_required
    def get(self, request, *args, **kwargs):
        
        self.resource = RedisHelper
        #Verify Request So That Going Forward We Can Use Request.GET Safely
#        required_keys = ['a']
        
        #Required Keys Vary By FeedbackType:
        action = request.GET.get('a')
        
        
        #Verify All Keys
#        for key in required_keys:
#            if request.GET.get(key) is None:
#                return Response(status = status.HTTP_400_BAD_REQUEST)
        
        #Define actions
        if action == "lookup":
            keys = [
                    'key',
                    'recursive'
                    ]
            
            list_keys = [    #Parameters that are expected to be lists
                         ]
            delimiter = '|'
            
            kwargs = self.deserialize_params(
                                             request, 
                                             val_keys=keys, 
                                             list_keys=list_keys,
                                             delimiter=delimiter)

            return Response(self.resource.lookup( **kwargs))
        
        elif action == "keys":
            keys = [
                    'pattern',
                    ]
            
            kwargs = self.deserialize_params(
                                             request, 
                                             val_keys=keys)

            return Response(self.resource.keys( **kwargs))
        
        elif action == "clear_keys_with_pattern":
            keys = [
                    'pattern',
                    ]
            
            kwargs = self.deserialize_params(
                                             request, 
                                             val_keys=keys)

            return Response(self.resource.clear_keys_with_pattern( **kwargs))
        
        elif action == "replicate":
            keys = [
                    'key'
                    ]
            
            list_keys = [    #Parameters that are expected to be lists
                         ]
            delimiter = '|'
            
            kwargs = self.deserialize_params(
                                             request, 
                                             val_keys=keys, 
                                             list_keys=list_keys,
                                             delimiter=delimiter)

            return Response(self.resource.replicate_master_db( **kwargs))
        
        
        #NEW REDIS COMMANDS: (ABOVE IS TO BE DEPRECATED)
        #Standard commands
        command = kwargs.get('command')
        key = request.GET.get('key')
        arg1 = request.GET.get('arg1')
        arg2 = request.GET.get('arg2')
        
        # Alternative 'natural' commands: (override arg1/arg2 structure)
        field = request.GET.get('field')
        member = request.GET.get('member')
        score = request.GET.get('score')
        value = request.GET.get('value')  # arg2 (wont work for get)
        
        #arg1 modifiers
        if field is not None:
            arg1 = field
        elif member is not None:
            arg1 = member
        
        #arg2 modifiers
        if value is not None:
            arg2 = value
        elif score is not None:
            arg2 = score
        
        
        #Process commands (Grouped by # of args taken)
        message = "Unable to process request"
        if command is not None and key is not None:
            if arg1 is None and arg2 is None:
                if command == 'hgetall':
                    return Response(redis.hgetall(key))
                elif command == 'smembers':
                    return Response(redis.smembers(key))
                elif command == 'zcard':
                    return Response(redis.zcard(key))
            elif arg2 is None:
                if command == 'hdel':
                    return Response(redis.hdel(key,arg1)) #key,field
                elif command == 'hget':
                    return Response(redis.hget(key,arg1)) #key,field
                elif command == 'zrank':
                    return Response(redis.zrank(key,arg1)) #key,member
                elif command == 'zrem':
                    return Response(redis.zrem(key,arg1)) #key,member
                elif command == 'zscore':
                    return Response(redis.zscore(key,arg1)) #key,member
            else:
                if command == 'hset':
                    return Response(redis.hset(key,arg1,arg2)) #key,field,value
                elif command == 'zadd':
                    return Response(redis.zadd(key,arg1,arg2)) #key,member,score
                elif command == 'zcount':
                    return Response(redis.zcount(key,arg1,arg2)) #key,min,max score
                elif command == 'zrange':
                    return Response(redis.zrange(key,arg1,arg2)) #key,start,stop
        
            message = "Redis Command '" + command + "' not supported at this time."
        
        return Response(message, status = status.HTTP_400_BAD_REQUEST)
        
    def deserialize_params (self, request, **kwargs):
        val_keys = kwargs.get('val_keys',[])
        list_keys = kwargs.get('list_keys',[])
#        dict_keys = kwargs.get('dict_keys',[])
        delimiter = kwargs.get('delimiter')
        
        
        kws = {}
        for key in val_keys:
            kws[key] = request.GET.get(key)
                
        for key in list_keys:
            kws[key] = request.GET.get(key).split(delimiter)
            
#        for key in dict_keys: #TODO
#            kws[key] = cls.deserialize_params(request)

        return kws

class PushView (APIView):
    def get(self, request, *args, **kwargs):
        """
        Validate GET request and send push notification
        Primarily used for testing push

        Required Params
        tokens
        message

        Optional Params
        custom_dict
        version

        """
        tokens = request.GET.get('tokens')
        message = request.GET.get('message')
        custom_dict = request.GET.get('custom_dict')
        version = request.GET.get('version')


        if tokens is None or message is  None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        tokens = tokens.split('|')

        #TODO: parse custom_dict str into a dict


        print("ATTEMPTING TO SEND PUSH NOTIFICATION:")
        print(message)

        push.notify(tokens=tokens, message=message, custom_dict=custom_dict, version=version)
        return Response({"Success": "Push Notification Sent",
                         "tokens": tokens,
                         "message": message}, status=status.HTTP_200_OK)


class ConfigurationView (APIView):
    def get(self, request, *args, **kwargs):
        """
        Validate GET request and return configuration info
        """
        _type = kwargs.get('type')
        
        if _type == 'id_field':
            resource = kwargs.get('resource')
            return Response(ResourceManager.id_field(resource, None), status=status.HTTP_200_OK)
            
        return Response(status=status.HTTP_400_BAD_REQUEST)
            
        
    def post(self, request, *args, **kwargs):
        """
        Validate POST request to create/overwrite api configuration.
        
        """
        _type = kwargs.get('type')
        
        if _type == 'id_field':
            decoded = request.body.decode('utf8')
            data = json.loads(decoded)
            if isinstance(data, dict):
                for resource, id_field in data.items():
                    ResourceManager.id_field(resource, id_field)
                return Response(ResourceManager.id_field(None, None), status=status.HTTP_201_CREATED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        
        return Response(status=status.HTTP_400_BAD_REQUEST)
            