'''
Created on Jun 26, 2014

@author: bryanfarris
'''
#Dates
from datetime import datetime #, date, time
from datetime import timedelta

#REDIS
from goldfish.store import redis

#Celery
from celery.task import Task
#from celery.registry import tasks
from celery.task import PeriodicTask
#from celery.task.control import discard_all

from goldfish.pushnotifs import push
#import goldfish.managers
from goldfish.managers import APIManager

#from goldfish.managers import UserManager, RedisHelper
#discard_all() #TODO: MAKE THIS A CALLABLE METHOD, not a default on startup

#Example of periodic task
#class SendInvite(PeriodicTask):
#    run_every = timedelta(minutes=10) #Batch send invites once every two hours or other time period
#    
#    def run(self, **kwargs):
#        UserManager.batch_send_activation_invites(batch_size = 2)
import importlib

class SendPushNotif (Task):
    
    def run (self, **kwargs):
        """
        Simple method that sends a push notification 
        
        Required Kwargs:
        -- Token
        -- Message
    
        Optional Kwargs
        -- Sound 
        -- Badge
        """
        
        #Gather Kwargs:
        tokens = kwargs.get('tokens')
        message = kwargs.get('message')
        custom_dict = kwargs.get('custom_dict')
        
        print("ATTEMPTING TO SEND PUSH NOTIFICATION:")
        print(message)


        push.notify(tokens=tokens, message=message, custom_dict=custom_dict)

class FireMethodTask (Task):
    
    def run (self, classname, method, **kwargs):
        """
        Fire any method.
        """
        cls = self.load_class(classname)
        getattr(cls, method)(**kwargs)
        print("Fire method:",cls,method,kwargs)
    
    

    def load_class(self, full_class_string):
        """
        dynamically load a class from a string
        """
    
        class_data = full_class_string.split(".")
        module_path = ".".join(class_data[:-1])
        class_str = class_data[-1]
    
        module = importlib.import_module(module_path)
        # Finally, we retrieve the Class
        return getattr(module, class_str)
        

class UpdateResource(Task):
    
    def run(self, query_params, data, overwrite, *args, **kwargs):
        APIManager.update_resource(query_params, data, overwrite, *args, **kwargs)
        