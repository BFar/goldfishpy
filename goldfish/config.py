'''
Created on Jan 6, 2015

@author: bryanfarris

Variables to define in Heroku Config:
'''
import os

### REQUIRED: ###



### OPTIONAL: ###

#GENERAL
ENVIRONMENT = os.environ.get('ENVIRONMENT','staging')  #(production, staging, local)
PRODUCTION_SERVER_URL = os.environ.get('PRODUCTION_SERVER_URL')
DEV_SERVER_URL = os.environ.get('DEV_SERVER_URL')

    
#REDIS & CELERY:
REDISTOGO_URL = os.environ.get('REDISTOGO_URL','redis://localhost:6379')

#TWILIO
TWILIO_ACCOUNT_SID = os.environ.get("TWILIO_ACCOUNT_SID","ACb99cedcc9f9eb1dddc06ea617ceccae4")
TWILIO_AUTH_TOKEN = os.environ.get("TWILIO_AUTH_TOKEN","5860062ea4ddb0a5afe59b18e449d99e")
TWILIO_FROM_NUM = os.environ.get("TWILIO_FROM_NUM","+14083354039")

#SENDY 
SENDY_API_KEY = os.environ.get("SENDY_API_KEY","gNGgbYwtq2uKyUFYNY8S")
SENDY_BASE_URL = os.environ.get("SENDY_BASE_URL","http://sendy.doMOARstuffs.com/sendy/")


SENDY_LIST_IDs = {'default':'892Uh9AMjE89ZPjrTFPTZ892bA'}#os.environ.get("SENDY_LIST_IDs",[]).split(',').append('892Uh9AMjE89ZPjrTFPTZ892bA')