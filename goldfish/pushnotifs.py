'''
Created on Jun 30, 2014

@author: bryanfarris

Supports sending push notifications
'''


#REDIS
from goldfish.store import redis
from goldfish.managers import APIManager
#
#from apns import APNs, Payload
from goldfish.apns import APNs, Payload, Frame
import os
from django.conf import settings
import time
import json
#from _pydev_xmlrpclib import Server

#NEW WAY OF IMPORTING:
#import importlib
#redis = importlib.import_module('goldfish.store.redis')
#APNs = importlib.import_module('apns.APNs')
#Payload = importlib.import_module('apns.Payload')


OUTSTANDING_PUSH_SET_KEY = 'outstanding_push_notifications'

#Primary class for initiating push notifs
class push():
    
    @classmethod
    def notify(cls, **kwargs):
        """
        Send a push notification based on server
        
        Required Kwargs:
        user or tokens
        message
        
        Optional Kwargs:
        custom_dict -- dict w/ custom params to pass
        version -- Flag indicating cert version
        
        """
        # Gather Kwargs
        message = kwargs.get('message')
        tokens = kwargs.get('tokens')
        user_id = kwargs.get('user_id')
        custom_dict = kwargs.get('custom_dict', {})
        version = kwargs.get('version')
        sound = kwargs.get('sound', 'StandardPush.caf')
        badge = 1

        print( "Message {0}".format( message ) )

        # USED FOR CHAT
        if user_id is not None:
            #RETRIEVE TOKENS FOR USER:
            args = ()
            request_params = {
                'user_id': user_id,
                'resource_tree': [{'users': 'user_id'}],
                'resource': 'tokens',
                'id_field': 'token_id',
                'style': 'verbose'
            }

            resources = cls.log_push_notif(
                            user_id = user_id,
                            message = message,
                            sent_at = round(time.time()),
                            custom_dict = custom_dict
                        )

            if isinstance(resources.get('notifications'), (str, int)):
                badge = int(resources.get('notifications'))

            message = cls.getShortMessage(message, badge, sound, custom_dict)

            response = APIManager.get_resource(*args, **request_params)
            if isinstance(response, dict) and 'TOKENS' in response.keys():
                tokens = response['TOKENS']
            else:
                return {'error': 'no device token'}

        print( "SENDING MESSAGE: {}".format( message ) )

        #TEMPORARILY COMMENTED OUT
        #Assign ID to push notif & add to custom_dict
        # push_id = redis.incr('push_id')
        # custom_dict['id'] = push_id
        
        
        #Log notif
        # cls.log_push_notif(
        #                    push_id=push_id,
        #                    message=message,
        #                    token=token,
        #                    custom_dict=custom_dict)

        
        print ("Sending push notification to tokens: ", tokens)


        # DETERMINE PUSH CERT VERSION
        if version is None:
            if os.environ.get('PUSH_VERSION'):
                version = os.environ.get('PUSH_VERSION')
            else:
                version = 'dev_sandbox'

        if 'sandbox' in version:
            sandbox_mode = True
        else:
            sandbox_mode = False

        # RETRIEVE PUSH CERT FILES
        filename = settings.PUSH_CERTIFICATES[version]


        certfile = os.path.join(settings.BASE_DIR, "certs", filename + "Cert.pem")
        keyfile = os.path.join(settings.BASE_DIR, "certs", filename + "Key.pem")

        print( "Certfile: {}".format( certfile ) )
        print( "Keyfile: {}".format( keyfile ) )

        # SETUP APNS SERVICE
        # apns_service = cls.apns_service(dev=dev)
        apns = APNs(use_sandbox=sandbox_mode, cert_file=certfile, key_file=keyfile)

        # Create Payload #TODO add payload size validation
        if custom_dict is not None:
            payload = Payload(alert=message, badge=badge, sound=sound, custom=custom_dict)
        else:
            payload = Payload(alert=message, badge=badge, sound=sound)



        # Retrieve Token Strings
        token_strings = []
        for t in tokens:
            # token = None
            if isinstance(t, dict):  # TOKEN OBJECT
                # RETRIEVE TOKEN STRING
                for k, v in t.items():
                    if k != 'token_id' and k != 'hashkey':
                        token_strings.append(v)
                        # token = v
                        break
            elif isinstance(t, str):  # TOKEN STRING
                token_strings.append(t)
                # token = t

        # Remove any duplicate tokens by converting to a set and back
        #token_strings = list(set(token_strings))

        token = None
        if len( token_strings ) > 0:

            # get the last token from the list
            token = token_strings[-1]

            # SEND PUSH TO TOKENS
            #for token in token_strings:

            # create frame to hold messages
            #frame = Frame()
            #TODO add validation of token objects

        if token is not None:
            #identifier = time.time() * 1000
            #expiry = time.time()+3600
            #priority = 10
            #frame.add_item( token, payload, identifier, expiry, priority)
            # send frame
            print( "Attempting to send notification to {0}".format( token ) )
            try:
                apns.gateway_server.send_notification( token, payload )
                print( "Notification Sent to: " + token + " with msg: " + message )
                return {'success': 'notification sent'}
            except Exception as e:
                print( e )
                return {'failed': 'notification failure'}

        print("Notification Send Failure: No Token Found")
        return {'error': 'no device token'}

#     @classmethod
#     def apns_service(cls, **kwargs):
#         """
#         Initiates and returns an instance of apns_service object
        
#         Optional Kwargs:
#         -- dev flag (True/False flag indicating dev mode)
#         """
#         #Gather Kwargs
#         dev = kwargs.get('dev',False)
#         if dev is None:
#             dev = False
#         apns_service = None
# #        if os.environ.get('production_server', 'No') == 'Yes' and dev is False:  
#         if os.environ.get('ENVIRONMENT') == 'production' and dev is False:
#             #Dev must be false as we use the dev flag to indicate that dev certs & dev tokens should be used for push notifs even if the server is in production_state for other users
#             #Production server detected
#             apns_service = APNs(use_sandbox=False, cert_file='apns-prod-cert.pem', key_file='apns-prod-key-noenc.pem')
#         else:
#             apns_service = APNs(use_sandbox=True, cert_file='apns-dev-cert.pem', key_file='apns-dev-key-noenc.pem')
        
#         return apns_service
    @classmethod
    def getShortMessage(cls, alert, badge, sound, custom_dict, content_available=False):
        '''
        reduce message if payload is too large
        '''
        content = alert
        while not cls.checksize(content, badge, sound, custom_dict, content_available):
            alert = alert[:-1]
            content = alert + '...'

        return content

    @classmethod
    def checksize(cls, alert, badge, sound, custom_dict, content_available=False):
        '''
        check if current payload occurs PayloadTooLargeError
        '''
        max_length = 256

        d = {}
        if alert:
            d['alert'] = alert
        if sound:
            d['sound'] = sound
        if badge is not None:
            d['badge'] = int(badge)
        if content_available:
            d.update({'content-available': 1})

        d = { 'aps': d }
        d.update(custom_dict)

        j = json.dumps(d, separators=(',',':'), ensure_ascii=False).encode('utf-8');

        if len(j) > max_length:
            return False

        return True
        
    @classmethod
    def log_push_notif (cls, **kwargs):
        """
        Logs record of push notif to our Server
        
        Required Kwargs:
        -- push_id
        -- token
        """
    
        #Gather Kwargs
        # push_id = str(kwargs.get('push_id'))
        # message = kwargs.get('message')
        # token = str(kwargs.get('token'))
        # custom_dict = kwargs.get('custom_dict')
        
        # token_set_key = OUTSTANDING_PUSH_SET_KEY + ':' + token
        # hash_key = OUTSTANDING_PUSH_SET_KEY + ':' + push_id
        # redis.sadd(OUTSTANDING_PUSH_SET_KEY,hash_key) #TRACK ALL OUTSTANDING NOTIFS
        # redis.sadd(token_set_key,hash_key) #TRACK OUSTANDING NOTIFS BY TOKEN THEY ARE INTEDNED FOR
        
        # #Create a hash & store all custom_dict keys
        # redis.hset(hash_key,'status','OUTSTANDING')
        # redis.hset(hash_key,'message',message)
        # redis.hset(hash_key,'token',token)
        
        # for kw in custom_dict.keys():
        #     v = custom_dict.get(kw)
        #     redis.hset(hash_key,kw,v)

        user_id = kwargs.pop('user_id')
        query_params = {}
        overwrite = False
        args = {}
        data = {
            'message' : kwargs.get('message'),
            'sent_at' : kwargs.get('sent_at'),
            'custom_dict': kwargs.get('custom_dict')
        }

        kwargs = {
            'resource_tree': [{'users': 'user_id'}],
            'resource': 'notifications',
            'id_field': 'notification_id',
            'user_id': user_id
        }
        
        resources = APIManager.update_resource(query_params, data, overwrite, *args, **kwargs)
        # return resources
        
        params = {
            'resource_tree': [],
            'resource': 'users',
            'id_field': 'user_id',
            'user_id': user_id,
            'style': 'default',
            'styles': {'default': {'notifications':'count'}}
        }

        user_obj = APIManager.get_resource(*args, **params)
        return user_obj
    
    # @classmethod
    # def outstanding_push_notifs (cls, **kwargs):
    #     """
    #     Returns a list of outstanding push notif objects retrieved from redis
        
    #     Optional Kwargs:
    #     -- output:  accepts 'count' or 'list' as params, defaults to returning a list
    #     -- token: will limit results to push notifs outstanding for a particular token
    #     """
        
    #     #Gather Kwargs:
    #     output = kwargs.get('output','list')
    #     token = kwargs.get('token')
        
        
    #     notifs = []
        
    #     #Determine set key
    #     notif_set_key = OUTSTANDING_PUSH_SET_KEY
    #     if token is not None:
    #         notif_set_key = OUTSTANDING_PUSH_SET_KEY + ':' + token
        
    #     for hash_key in redis.smembers(notif_set_key):
    #         notifs.append(redis.hgetall(hash_key))
        
        
    #     # Check output param to see what to return
    #     if output == 'count':
    #         return len(notifs)
        
    #     return notifs
        
    # @classmethod
    # def clear_push_notif_log (cls, **kwargs):
    #     """
    #     Clears record of push notif from our Server log
        
    #     Required Kwargs:
    #     -- push_id
    #     -- token
    #     """
    
    #     #Gather Kwargs
    #     push_id = str(kwargs.get('push_id'))
    #     token = str(kwargs.get('token'))
        
    #     #Determine set key and hash_key to remove:
    #     token_set_key = OUTSTANDING_PUSH_SET_KEY + ':' + token
        
    #     #Remove hash_key from set and delete
    #     hash_key = OUTSTANDING_PUSH_SET_KEY + ':' + push_id
    #     redis.srem(token_set_key,hash_key)
    #     redis.srem(OUTSTANDING_PUSH_SET_KEY,hash_key)
    #     redis.delete(hash_key)
        
        
        