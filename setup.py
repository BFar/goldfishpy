import os
from setuptools import setup, find_packages


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name = "goldfish",
    version = "0.0.0",
    author = "Bryan Farris",
    author_email = "bryanfarris@gmail.com",
    description = "Goldfish Django Framework",
    license = "Copyright Goldfish Productions",
    url = "http://www.goldfishproductions.com",
#    long_description=read('README'),
    packages=find_packages(),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2.7",
        "Framework :: Django",
    ],
    install_requires=[
        'django>=1.4',
        'djangorestframework',
        'twilio',
#        'vobject'
    ],
    include_package_data=True,

)